/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.job;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolTaskExecutor {
	
	/* 缺省大小为 cpu个数的 2倍 */
	static final int DEFAULT_POOL_SIZE = Runtime.getRuntime().availableProcessors() * 2;
	/* 缺省最大线程数为 cpu个数的4倍  */
	static final int DEFAULT_MAX_SIZE = Runtime.getRuntime().availableProcessors() * 4;
	
	static final long DEFAULT_KEEP_ALIVE_TIME = 60L; //默认60s
	
	private ThreadPoolExecutor threadPoolExecutor;
	private int corePoolSize;
	private int maxPoolSize;
	private long keepAliveTime;
	
	private BlockingQueue<Runnable> workQueue;
	
	
	public ThreadPoolTaskExecutor() {
		this(DEFAULT_POOL_SIZE, DEFAULT_MAX_SIZE,
				DEFAULT_KEEP_ALIVE_TIME, TimeUnit.SECONDS, new LinkedBlockingDeque<Runnable>());
	}
	
    public ThreadPoolTaskExecutor(int corePoolSize, int maxPoolSize, 
    		                      long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
    	this.corePoolSize = corePoolSize;
    	this.maxPoolSize = maxPoolSize;
    	this.keepAliveTime = keepAliveTime;
    	this.workQueue = workQueue;
    	this.threadPoolExecutor = createThreadPoolExecutor(corePoolSize, maxPoolSize, 
    			keepAliveTime, unit, workQueue);
	}

	private ThreadPoolExecutor createThreadPoolExecutor(int corePoolSize, int maxPoolSize, 
            long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
		return new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepAliveTime, unit, workQueue);
	}

	public void setWorkQueue(BlockingQueue<Runnable> workQueue) {
		this.workQueue = workQueue;
	}
	
	public BlockingQueue<Runnable> getWorkQueue() {
		return workQueue;
	}
	
	public long getKeepAliveTime() {
		return keepAliveTime;
	}

	public void setKeepAliveTime(long keepAliveTime) {
		this.keepAliveTime = keepAliveTime;
	}

	public ThreadPoolExecutor getThreadPoolExecutor() {
		return threadPoolExecutor;
	}


	public void setThreadPoolExecutor(ThreadPoolExecutor threadPoolExecutor) {
		this.threadPoolExecutor = threadPoolExecutor;
	}

	public int getCorePoolSize() {
		return corePoolSize;
	}

	public void setCorePoolSize(int corePoolSize) {
		this.corePoolSize = corePoolSize;
	}

	public int getMaxPoolSize() {
		return maxPoolSize;
	}


	public void setMaxPoolSize(int maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}
	
	public void execute(Runnable task) {
		Executor executor = getThreadPoolExecutor();
		try {
			executor.execute(task);
		}
		catch (Exception ex) {
			
		}
	}

}
