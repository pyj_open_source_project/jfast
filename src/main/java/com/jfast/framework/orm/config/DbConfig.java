/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.orm.config;

import com.jfast.framework.aop.ConnectionInvocationHandler;
import com.jfast.framework.exception.DataSourceException;
import com.jfast.framework.orm.dataSource.DataSourceFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class DbConfig {
	
	private final ThreadLocal<Connection> threadLocal = new ThreadLocal<Connection>();
	private DataSourceFactory dataSourceFactory;
	
	public DbConfig(DataSourceFactory dataSourceFactory){
		this.dataSourceFactory = dataSourceFactory;
	}
	
	public DataSourceFactory getDataSourceFactory() {
		return dataSourceFactory;
	}

	public void setDataSourceFactory(DataSourceFactory dataSourceFactory) {
		this.dataSourceFactory = dataSourceFactory;
	}

	public final ThreadLocal<Connection> getThreadLocal() {
		return threadLocal;
	}
	
	public final Connection getThreadLocalConnection() {
		return threadLocal.get();
	}
	
	public final void setThreadLocalConnection(Connection connection) {
		threadLocal.set(connection);
	}
	
	public final void removeThreadLocalConnection() {
		threadLocal.remove();
	}
	
	public final Connection getConnection() throws SQLException{
		Connection connection = threadLocal.get();
		if (connection != null)
			return connection;
		return new ConnectionInvocationHandler<Connection>(getDataSourceFactory().getDataSource().getConnection()).getTarget();
	}
	
	public void close(Connection connection){
		if (threadLocal.get() == null)
		  if (connection != null)
			try {connection.close();} catch (SQLException e) {throw new DataSourceException(e);}
	}
}
