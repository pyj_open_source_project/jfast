/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.orm.dataSource;

import com.jfast.framework.log.Logger;
import com.jfast.framework.orm.config.DataSourceConfig;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

public class HikariDataSourceFactory extends AbstractDataSource {

	private HikariDataSource dataSource;
	private static final Logger logger = Logger.getLogger(HikariDataSourceFactory.class);
	
	public HikariDataSourceFactory(DataSourceConfig dataSourceConfig) {
		super(dataSourceConfig);
	}

	@Override
	public DataSource getDataSource() {
		return dataSource;
	}

	@Override
	public void initDataSource(DataSourceConfig dataSourceConfig) {
		try {
			HikariConfig hikariConfig = new HikariConfig();
	        hikariConfig.setJdbcUrl(dataSourceConfig.getUrl());
	        hikariConfig.setUsername(dataSourceConfig.getUsername());
	        hikariConfig.setPassword(dataSourceConfig.getPassword());
	        hikariConfig.setDriverClassName(dataSourceConfig.getDriverClassName());
	        dataSource = new HikariDataSource(hikariConfig);
		} catch (Exception e) {
			dataSource = null;
			logger.error("HikariDataSource loading error",e);
		}
	}
}
