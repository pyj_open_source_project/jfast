/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.orm;

import java.util.Map;

/**
 * 表信息
 * @author zengjintao
 * @version 1.0
 * @createTime 2018年1月12日上午10:19:20
 */
public class TableInfo {
	
	private String tableName;
	
	private String[] primaryKeys;
	
	private Class<?> tableClass;
	
	private Map<String, String> columnMapping;

	public TableInfo(String tableName, String[] primaryKeys, Class<?> tableClass, Map<String,String> columnMapping) {
		this.tableName = tableName;
		this.primaryKeys = primaryKeys;
		this.tableClass = tableClass;
		this.columnMapping = columnMapping;
	}

	public TableInfo() {
		
	}

	public Map<String, String> getColumnMapping() {
		return columnMapping;
	}

	public void setColumnMapping(Map<String, String> columnMapping) {
		this.columnMapping = columnMapping;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Class<?> getTableClass() {
		return tableClass;
	}

	public String[] getPrimaryKeys() {
		return primaryKeys;
	}

	public void setPrimaryKeys(String[] primaryKeys) {
		this.primaryKeys = primaryKeys;
	}

	public void setTableClass(Class<?> tableClass) {
		this.tableClass = tableClass;
	}
}
