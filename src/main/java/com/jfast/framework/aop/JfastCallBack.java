/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.aop;


import java.lang.reflect.Method;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

public class JfastCallBack implements MethodInterceptor {

	public JfastCallBack() {
		
	}
	
	/***
	 * object 目标对象
	 * method 目标方法
	 */
	@Override
	public Object intercept(Object object, Method method, Object[] args, MethodProxy methodProxy) 
			throws Throwable {
		return null;
		/*if (injectInters == null) {
			return methodProxy.invokeSuper(object, args);
		}
		Invocation invocation = new Invocation(object, method, args, methodProxy, null);
		invocation.invoke();
		return invocation.getReturnValue();*/
	}
}
