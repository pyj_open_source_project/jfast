/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.aop;

import java.util.ArrayList;
import java.util.List;



/**
 * InteceptorManager
 * @author zengjintao
 * @version 1.0
 * @createTime 2017年10月28日下午3:29:26
 */
public class InterceptorManager {

	public static final Interceptor[] NULL_INTERS = new Interceptor[0];
	
	private List<Interceptor> inteceptors = new ArrayList<>();
	
	private static final InterceptorManager inteceptorManager = new InterceptorManager();
	
	public static InterceptorManager getInteceptorManager() {
		return inteceptorManager;
	}
	
	private InterceptorManager() {
		
	}
	
	public void add(Interceptor inteceptor) {
		inteceptors.add(inteceptor);
	}
	
    
    public List<Interceptor> getInterceptors() {
		return inteceptors;
	}
    
    public Interceptor[] getInterceptorsToArray() {
    	Interceptor[] interceptors = new Interceptor[getInterceptors().size()];
    	for (int i = 0; i < getInterceptors().size(); i++) {
    		interceptors[i] = getInterceptors().get(i);
    	}
		return interceptors;
    }
}
