/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.log;


public abstract class Logger {

	private static LogFactory defaultLogFactory = null;
	
	static {
		init();
	}
	
	public static Logger getLogger(String name){
		return defaultLogFactory.getLogger(name);
	}
	
	public static LogFactory getLogFactory() {
		return defaultLogFactory;
	}

	public static void setLogFactory(LogFactory logFactory) {
		if (logFactory == null) {
			throw new IllegalArgumentException("defaultLogFactory can not be null.");
		}
		Logger.defaultLogFactory = logFactory;
	}

	public static void init() {
		if (defaultLogFactory == null) {
			try {
				Class.forName("org.apache.log4j.Logger");
				Class<?> log4jLogFactoryClass = Class.forName("com.jfast.framework.log.Log4JLogFactory");
				defaultLogFactory = (LogFactory)log4jLogFactoryClass.newInstance();
			} catch (Exception e) {
				defaultLogFactory = new JdkLogFactory();
			}
		}
	}

	public static Logger getLogger(Class<?> cla){
		return defaultLogFactory.getLogger(cla);
	}
	
    public abstract void debug(String message);
	
	public abstract void debug(String message, Throwable t);
	
	public abstract void info(String message);
	
	public abstract void info(String message, Throwable t);
	
    public abstract void warn(String message);
	
	public abstract void warn(String message, Throwable t);
	
	public abstract void error(String message);
	
	public abstract void error(String message, Throwable t);
	
	public abstract void fatal(String message);
	
	public abstract void fatal(String message, Throwable t);
	
	public abstract boolean isDebugEnabled();

	public abstract boolean isInfoEnabled();

	public abstract boolean isWarnEnabled();

	public abstract boolean isErrorEnabled();
	
	public abstract boolean isFatalEnabled();
}
