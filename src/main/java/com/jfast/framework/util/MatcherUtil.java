package com.jfast.framework.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 正则表达式工具类
 * @author zengjintao
 * @version 1.0
 * @createTime 2017年12月31日下午3:36:05
 */
public class MatcherUtil {

	
	/**
	 * 日期校验
	 * @param time
	 * @return
	 */
	public static boolean timeMatcher(String time){
		Pattern pattern = Pattern.compile("(\\d{4})-(0\\d{1}|1[0-2])-(0\\d{1}|[12]\\d{1}|3[01])");
		Matcher mat = pattern.matcher(time);
		return mat.matches();
	}
	/**
	 * 手机号码校验
	 * @param mobile
	 * @return
	 */
	public static boolean moblieMatcher(String mobile){
		Pattern pattern = Pattern.compile("13\\d{9}||18\\d{9}||15\\d{9}||17\\d{9}");
		Matcher mat = pattern.matcher(mobile);
		return mat.matches();
	}
	
	/**
	 * 数值校验(整数或小数(最多两位小数))
	 * @param number
	 * @return
	 */
	public static boolean numberMatcher(String number){
		Pattern pattern = Pattern.compile("^[0-9]+\\.?[0-9]{2}*$");
		Matcher mat = pattern.matcher(number);
		return mat.matches();
	}
	
	/**
	 * 匹配actionKey
	 * 例如: /jfast/jfat/2/2
	 * @param number
	 * @return
	 */
	public static boolean actionKeyMatcher(String actionKey){
		Pattern pattern = Pattern.compile("(/\\w+)+?(/[0-9]+?)+?");//   /login/2/3
		Matcher mat = pattern.matcher(actionKey);
		return mat.matches();
	}
	
	/**
	 * 匹配actionKey
	 *  例如: /jfast/jfat/{id}/{id}
	 * @param number
	 * @return
	 */
	public static boolean actionUrlMatcher(String actionUrl){
		Pattern pattern = Pattern.compile("(/\\w+)+?(/{1}\\{\\w+\\})+?");//   /login/2/3
		Matcher mat = pattern.matcher(actionUrl);
		return mat.matches();
	}
	
	public static void main(String[] args) {
		String path = "/login/login/{id}/{id}";
		
	//	System.out.println(actionUrlMatcher(path));
		//path =  path.substring(0, path.indexOf("{") - 1);
		String pathKey = path.substring(path.indexOf("{"), path.length());
		String pathParam = pathKey.replaceAll("[{}]","");
		String[] params = pathParam.split("/");
		System.out.println(params);
//		int i = path.indexOf("{");
//		System.err.println(path.substring(i,path.length()));
//		System.err.println(path.substring(i,path.length()).split("/")[0]);;
//		Pattern pattern = Pattern.compile("(/\\w+)+?(/{1}\\{\\w+\\})+?");
//		
//		System.out.println(pattern.matcher(path).matches());
//		System.out.println(actionUrlMatcher(url));
		//System.err.println(url.replaceAll("/\\d+", ""));
		
		
	}
}
