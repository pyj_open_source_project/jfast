/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.server.tomcat;






import com.jfast.framework.server.JfastServer;

public class TomcatServer extends JfastServer {
	
    static final String DEFAULT_PROTOCOL = "org.apache.coyote.http11.Http11NioProtocol";
	
	//private Tomcat tomcat;
	
	public TomcatServer(){
		
	}
	
	@Override
	public void doStart() {
//		tomcat = new Tomcat();
//		String webappDirLocation = jfastServerConfig.getResourceLocation();
//		File baseDir = new File(webappDirLocation);
//		tomcat.setBaseDir(baseDir.getAbsolutePath());
//		Connector connector = new Connector(DEFAULT_PROTOCOL);
//		connector.setPort(jfastServerConfig.getPort());
//		tomcat.getService().addConnector(connector);
//		tomcat.setConnector(connector);
//		tomcat.getHost().setAutoDeploy(true);
//		try {
//			StandardContext context = new StandardContext();
//			AprLifecycleListener listener = new AprLifecycleListener();  
//			context.addApplicationLifecycleListener(listener);
//			context.addApplicationEventListener(new JfastWebListener());
//			context.setPath(getJfastServerConfig().getContextPath());
//			tomcat.addServlet(context, "coreHttpServlet", new CoreHttpServlet());
//			tomcat.getHost().addChild(context);
//			tomcat.start();
//			tomcat.getServer().await();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}


	@Override
	public void doReStart() {
		
	}
}
