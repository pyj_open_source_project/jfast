/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework;




import com.jfast.framework.aop.GuiceManager;
import com.jfast.framework.server.IServer;
import com.jfast.framework.server.ServerFactory;
import com.jfast.framework.web.cache.redis.JfastRedis;
import com.jfast.framework.web.cache.redis.JfastRedisManager;
import com.jfast.framework.web.config.JfastConfig;
import com.jfast.framework.web.config.JfastConfigManager;
import com.jfast.framework.web.core.MethodMappingHandler;
import com.jfast.framework.web.handler.Handler;
import com.jfast.framework.web.handler.HandlerManager;



public class Jfast {
	
	private static String contextPath ;
	
	public static void run(String[] args){
		IServer jfastServer = ServerFactory.getServerFactory().createServer();
		jfastServer.start();
	}
	
	public static String getContextPath() {
		return contextPath;
	}

	public static void setContextPath(String contextPath) {
		Jfast.contextPath = contextPath;
	}
	
	public static JfastConfig getJfastConfig(){
		return JfastConfigManager.createConfigBean(JfastConfig.class);
	}
	
	public static JfastRedis getJfastRedis(){
		return JfastRedisManager.getJfastRedisManager().getRedis();
	}

	
	/**
     * 获取被增强的，可以使用AOP注入的实体类
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T getBean(Class<T> clazz) {
        return GuiceManager.getGuiceManger().getInjector().getInstance(clazz);
    }
	
    /**
     * guice依赖注入
     * @param object
     */
	public static void injectMembers(Object object){
		GuiceManager.getGuiceManger().getInjector().injectMembers(object);
	}
	
	public static final HandlerManager getHandlerManger(){
		return HandlerManager.getHandlerManager();
	}
	
	public static final MethodMappingHandler getActionHandler(){
		return MethodMappingHandler.getMethodMappingHandler();
	}
	
	public static final Handler getHandler(){
		return getHandlerManger().getHandlerFactory().getHandler(getHandlerManger().getHandlers(), getActionHandler());
	}
	
}
