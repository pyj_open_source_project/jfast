/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.cache;


import com.jfast.framework.web.cache.ehcache.JfastEhcacheCache;
import com.jfast.framework.web.cache.redis.JfastRedisCache;
import com.jfast.framework.web.config.JfastConfig;

/**
 * Jfast缓存管理器
 * @author zengjintao
 * @version 1.0
 * @createTime 2017年11月2日下午8:21:31
 */
public final class JfastCacheManager {

	private JfastCache jfastCache;

	private static final JfastCacheManager jfastCacheManager = new JfastCacheManager();
	
	private JfastCacheManager(){
		
	}

	public JfastCache getJfastCache() {
		return jfastCache;
	}

	public void setJfastCache(JfastCache jfastCache) {
		this.jfastCache = jfastCache;
	}

	public static JfastCacheManager getCacheManager() {
		return jfastCacheManager;
	}

	public JfastCache getJfastCache(JfastConfig jfastConfig){
		if(jfastCache == null)
			jfastCache = getJfastCache(jfastConfig.getCacheType());
		return  jfastCache;
	}

	private JfastCache getJfastCache(String cacheType) {
		switch (cacheType){
			case JfastConfig.EH_CACHE_TYPE :
				return  new JfastEhcacheCache();
			case JfastConfig.REDIS_CACHE_TYPE :
				return  new JfastRedisCache();
		}
		return  null;
	}
}
