package com.jfast.framework.web.cache.ehcache;

import com.jfast.framework.web.annotation.ConfigurationProperties;

@ConfigurationProperties(prefix = "jfast.cache.ehCache")
public class JfastEhCacheConfig {

    private String configFileName;

    public String getConfigFileName() {
        return configFileName;
    }

    public void setConfigFileName(String configFileName) {
        this.configFileName = configFileName;
    }
}
