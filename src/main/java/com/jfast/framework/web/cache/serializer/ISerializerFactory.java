package com.jfast.framework.web.cache.serializer;

import com.jfast.framework.web.config.JfastConfigManager;

public class ISerializerFactory {

	private static ISerializerFactory factory = new ISerializerFactory();
	
	public static ISerializerFactory getISerializerFactory() {
		return factory;
	}
	
	public ISerializer getISerializer(){
		JfastSerializerConfig config = JfastConfigManager.createConfigBean(JfastSerializerConfig.class);
		return getISerializer(config.getType());
	}

	private ISerializer getISerializer(String type) {
		switch (type) {
			case JfastSerializerConfig.FST:
				return new FstSerializer();
			case JfastSerializerConfig.JDK:
		        return new JdkSerializer();
	    }
	    return null;
	}
	
}
