package com.jfast.framework.web.cache;

import java.util.List;

public abstract class JfastCache {

    protected String buildKey(String cacheName, Object key){
        if(key instanceof  Number || key instanceof String){
             return   String.format("%s:%s",cacheName,key);
        }
        return  String.format("%s:%s",cacheName,key.getClass().getSimpleName());
    }

    public abstract void put(String cacheName,Object key,Object value);

    public abstract void set(String cacheName,Object key,Object value);

    public abstract <T> T get(String cacheName,Object key);

    @SuppressWarnings("rawtypes")
	public abstract List getKeys(String cacheName);

    public abstract void remove(String cacheName,Object key);
}
