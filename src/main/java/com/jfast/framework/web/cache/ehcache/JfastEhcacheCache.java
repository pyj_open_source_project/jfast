package com.jfast.framework.web.cache.ehcache;

import com.jfast.framework.kit.PathKit;
import com.jfast.framework.kit.StrKit;
import com.jfast.framework.web.cache.JfastCache;
import com.jfast.framework.web.config.JfastConfigManager;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.util.List;


public class JfastEhcacheCache extends JfastCache {

    private CacheManager cacheManager;

    public JfastEhcacheCache() {
        JfastEhCacheConfig jfastConfig = JfastConfigManager.createConfigBean(JfastEhCacheConfig.class);
        if (StrKit.isEmpty(jfastConfig.getConfigFileName())) {
            cacheManager = CacheManager.create();
        } else {
            String configPath = jfastConfig.getConfigFileName();
            if (!configPath.startsWith("/")) {
                configPath = PathKit.getRootClassPath() + "/" + configPath;
            }
            cacheManager = CacheManager.create(configPath);
        }
    }

    public void addCache(String cacheName) {
        cacheManager.addCache(cacheName);
    }

    @Override
    public void put(String cacheName, Object key, Object value) {

    }

    @Override
    public void set(String cacheName, Object key, Object value) {

    }

    @SuppressWarnings("unchecked")
	@Override
    public <T> T get(String cacheName, Object key) {
        Element element = cacheManager.getCache(cacheName).get(key);
        return element != null ? (T) element.getObjectValue() : null;
    }

    @SuppressWarnings("rawtypes")
	@Override
    public List getKeys(String cacheName) {
        return null;
    }

    @Override
    public void remove(String cacheName, Object key) {

    }

    @SuppressWarnings("unused")
	private void createCache(String cacheName) {
         cacheManager.addCache(cacheName);
    }

    @SuppressWarnings("unused")
	private Cache getCache(String cacheName) {
        Cache cache = cacheManager.getCache(cacheName);
        return  cache;
    }
}
