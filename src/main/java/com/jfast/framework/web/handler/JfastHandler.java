package com.jfast.framework.web.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.jfast.framework.web.session.RequestContext;

public class JfastHandler extends Handler {

	private static final RequestContext requestManger = RequestContext.getRequestManager();
	
	@Override
	public void doHandler(String target, HttpServletRequest request, HttpServletResponse response) {
		requestManger.handle(request, response);
		nextHandler.doHandler(target, request, response);
	}
}
