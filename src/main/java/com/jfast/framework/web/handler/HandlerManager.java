package com.jfast.framework.web.handler;

import java.util.ArrayList;
import java.util.List;

import com.jfast.framework.Jfast;


/**
 * Handler管理器
 * @author zengjintao
 * @version 1.0
 * @createTime 2017年12月1日下午9:16:25
 */
public class HandlerManager {
	
	private static final HandlerManager handlerManager = new HandlerManager();
	private static List<Handler> handlers = new ArrayList<>();
	
	private HandlerManager(){}
	
	public static HandlerManager getHandlerManager() {
		return handlerManager;
	}
	
	public void add(Handler handler){
		handlers.add(handler);////方便为handler插件的自动注入功能
		Jfast.injectMembers(handler);
	}
	
	public List<Handler> getHandlers() {
		return handlers;
	}
	
	public HandlerFactory getHandlerFactory(){
		return HandlerFactory.getHandlerfactory();
	}
}
