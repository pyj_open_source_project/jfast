/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.config;

import java.util.HashSet;
import java.util.Set;

import com.jfast.framework.kit.StrKit;
import com.jfast.framework.util.ClassScanner;
import com.jfast.framework.web.annotation.ConfigurationProperties;


@ConfigurationProperties(prefix="jfast.config")
public class JfastConfig {

	public static final String DEFAULT_VIEW_PATH = "/";
    public static final String JSP = "jsp";
    public static final String VELOCITY = "velocity";
    public static final String ENJOY = "enjoy";
	public static final String EH_CACHE_TYPE = "ehCache";
	public static final String REDIS_CACHE_TYPE = "redisCache";

    private String baseViewPath = "";
    
    private String urlSeparator = "-";//url分隔符    /jfast/2-3-5
    
    private String resourcePath = "/resources/*";//静态资源路径
	
	private String viewType = VELOCITY; //默认使用velocity模板引擎

	private String filePath;//文件上传服务器地址
    
    private String suffix = ".html";//视图后缀
    
    private boolean autoProxy = false; //默认使用jdk动态代理

	@SuppressWarnings("unused")
	private String jars;

	private boolean hasCache = false;

	private String cacheType = EH_CACHE_TYPE;
    
    private Set<String> set = new HashSet<>();

	public String getCacheType() {
		return cacheType;
	}

	public void setCacheType(String cacheType) {
		this.cacheType = cacheType;
	}

	public void setSet(Set<String> set) {
		this.set = set;
	}

	/**
     * 设置要扫描的jar包(多个jar包以,隔开)
     * @param jars
     */
    public void setJars(String jars) {
    	if(jars.contains(",")){
			for(String jar : StrKit.spilt(jars)){
				set.add(jar);
			}
		}else{
			set.add(jars);
		}
    	ClassScanner.addJars(set);
	}
	public void setCache(boolean hasCache) {
		this.hasCache = hasCache;
	}

	public boolean hasCache() {
		return hasCache;
	}

	public void setUrlSeparator(String urlSeparator) {
		this.urlSeparator = urlSeparator;
	}
	
	public String getUrlSeparator() {
		return urlSeparator;
	}
	
	public boolean isAutoProxy() {
		return autoProxy;
	}

	public void setAutoProxy(boolean autoProxy) {
		this.autoProxy = autoProxy;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

    public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	
    public Set<String> getSet() {
		return set;
	}

	public String getViewType() {
		return viewType;
	}

	/**
	 * 设置视图类型
	 * @param viewType
	 */
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	
	public String getBaseViewPath() {
		return baseViewPath;
	}

	/**
	 * 设置页面存放路径
	 * @param baseViewPath
	 * @return
	 */
	public JfastConfig setBaseViewPath(String baseViewPath) {
		this.baseViewPath = baseViewPath;
		return this;
	}
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
}

