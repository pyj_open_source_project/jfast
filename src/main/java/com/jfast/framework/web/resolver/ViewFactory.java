/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.resolver;


import java.lang.reflect.Method;

import com.jfast.framework.Jfast;
import com.jfast.framework.exception.ResolverException;
import com.jfast.framework.web.annotation.ResponseJson;
import com.jfast.framework.web.config.JfastConfig;

import javax.servlet.ServletContext;

/**
 * 
 * @author zengjintao
 * @version 1.0
 * @createTime 2017年10月21日上午11:39:02
 */
public class ViewFactory {
	
	private ViewFactory(){
		
	}
	
	private static final ViewFactory viewFactory = new ViewFactory();
	
	public static ViewFactory getViewFactory() {
		return viewFactory;
	}
	
	public ViewResolver getViewResolver(Method method, Object returnValue) {
		ResponseJson responseJson = method.getAnnotation(ResponseJson.class);
		if (responseJson != null) {
			return new JsonViewResolver(returnValue);
		}
		else if (method.getReturnType() == void.class) {
			return new NullResolver();
		}
		
		try {
			String location = (String) returnValue;
			if (location.startsWith("redirect:")) {
				return new RedirectResolver(location);
			} else {
				switch (Jfast.getJfastConfig().getViewType()) {
					case JfastConfig.JSP:
						return new JspViewResolver();
					case JfastConfig.VELOCITY:
						return new VelocityResolver();
					case JfastConfig.ENJOY:
						return new EnjoyResolver();
					default:
						return new NullResolver();
			    }
			}
		} catch (Exception e) {
			throw new ResolverException("can not create viewResolver", e);
		}
	}

	public void initVelocityResolver(ServletContext servletContext){
		VelocityResolver.init(servletContext);
	}
}
