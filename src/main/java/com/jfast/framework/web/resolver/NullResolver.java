package com.jfast.framework.web.resolver;

/**
 * 空视图处理(用于处理Controller void方法)
 * @author zengjintao
 * @version 1.0
 * @createTime 2018年7月11日下午3:34:36
 */
public class NullResolver extends ViewResolver {

	@Override
	public final void render() {
		
	}
}
