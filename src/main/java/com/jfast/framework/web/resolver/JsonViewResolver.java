/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.resolver;

import java.io.IOException;
import java.io.PrintWriter;

import com.alibaba.fastjson.JSON;
import com.jfast.framework.exception.ResolverException;



public class JsonViewResolver extends ViewResolver {

	private String jsonText;
	
	private static final String contentType = "application/json; charset=" + getEncoding();

	public JsonViewResolver(Object returnValue) {
		this.jsonText = JSON.toJSONString(returnValue);
	}

	@Override
	public void render() {
		PrintWriter writer = null;
		try {
			response.setHeader("Pragma", "no-cache");	// HTTP/1.0 caches might not implement Cache-Control and might only implement Pragma: no-cache
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			response.setContentType(contentType); //设置contentType
			writer = response.getWriter();
			writer.write(jsonText);
	        writer.flush();
		} catch (IOException e) {
			throw new ResolverException(e);
		}
	}
}
