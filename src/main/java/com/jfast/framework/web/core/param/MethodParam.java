package com.jfast.framework.web.core.param;


public interface MethodParam {
	Object getParam();
}
