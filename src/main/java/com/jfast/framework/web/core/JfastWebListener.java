/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.core;


import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;
import com.jfast.framework.Jfast;
import com.jfast.framework.kit.PathKit;
import com.jfast.framework.kit.StrKit;
import com.jfast.framework.web.resolver.ViewFactory;

@WebListener
public class JfastWebListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent event) {
		initWebApplicationContext(event);
		ServletContext servletContext = event.getServletContext();
		registerJspServlet(servletContext);
		initPath(servletContext);
	}

	
	private void initWebApplicationContext(ServletContextEvent event) {
		ServletContext servletContext = event.getServletContext();
		initPath(servletContext);
		initJfastConfig();
		registerJspServlet(event.getServletContext());
	}

	private void initJfastConfig() {
		Config.getConfig().init();
	}

	private void initPath(ServletContext servletContext) {
		PathKit.setWebRootPath(servletContext.getRealPath("/"));
		ViewFactory.getViewFactory().initVelocityResolver(servletContext);
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		
	}
	
	/**
	 * 注册servlet
	 * @param context
	 */
	private void registerJspServlet(ServletContext context) {
        ServletRegistration jspServlet = context.getServletRegistration("jsp");
        jspServlet.addMapping("/index.jsp");
        String jspPath = Jfast.getJfastConfig().getBaseViewPath();
        if (StrKit.isNotEmpty(jspPath)) {
            jspServlet.addMapping(jspPath + "*");
            String resourcePath = Jfast.getJfastConfig().getResourcePath();
            jspServlet.addMapping(resourcePath + "/*");
        }
    }
}
