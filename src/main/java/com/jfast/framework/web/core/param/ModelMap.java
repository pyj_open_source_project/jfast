/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.web.core.param;

import java.util.HashMap;


public class ModelMap<K, V> extends HashMap<K, V> {
	
	private static final long serialVersionUID = 1L;

	public void set(K key,V value){
		super.put(key, value);
	}
	
	public V get(Object key) {
		return super.get(key);
	}
	
	@SuppressWarnings("unchecked")
	public V get(K key, Object defaultValue) {
		return super.get(key) == null ? (V) defaultValue : super.get(key);
	}
	
	public Integer getInt(K key) {
		V value = super.get(key);
		return value != null ? (Integer)value : null;
	}
	
	public String getStr(K key) {
		V value = super.get(key);
		return value != null ? value.toString() : null;
	}
	
	public Boolean getBoolean(K key) {
		V value = super.get(key);
		return value != null ? (Boolean)value : null;
	}
	
	public Number getNumber(K key) {
		return (Number)get(key);
	}
	
	public Long getLong(K key) {
		Number n = (Number)get(key);
		return n != null ? n.longValue() : null;
	}
	
	public java.util.Date getDate(K key) {
		return (java.util.Date)super.get(key);
	}
	
	public java.sql.Time getTime(K key) {
		return (java.sql.Time)super.get(key);
	}
	
	public java.sql.Timestamp getTimestamp(K key) {
		return (java.sql.Timestamp)super.get(key);
	}
	
	public Double getDouble(K key) {
		Number n = (Number)super.get(key);
		return n != null ? n.doubleValue() : null;
	}
	
	/**
	 * Get attribute of mysql type: float
	 */
	public Float getFloat(K key) {
		Number n = (Number)super.get(key);
		return n != null ? n.floatValue() : null;
	}
	
	public Short getShort(K key) {
		Number n = (Number)super.get(key);
		return n != null ? n.shortValue() : null;
	}
	
	public Byte getByte(K key) {
		Number n = (Number)super.get(key);
		return n != null ? n.byteValue() : null;
	}
	
	public java.math.BigDecimal getBigDecimal(K key) {
		return (java.math.BigDecimal)super.get(key);
	}
	
	public byte[] getBytes(K key) {
		return (byte[])super.get(key);
	}
}
