/**
 * Copyright (c) 2017-2018, zengjintao (1913188966@qq.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfast.framework.kit;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StrKit {
	
	/**
	 * 判断字符串是否为空IsEmpty()
	 */
	public static boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0 ? true : false;
	}
	
	public static boolean isBank(String str) {
		return str == null ? true : false;
	}
	
	public static boolean isEmpty(String[] properties) {
		return properties == null || properties.length == 0 ? true : false;
	}
	
	public static boolean isNotBank(String str) {
		return str != null && str.length() > 0 ? true:false;
	}
	
	public static boolean isEmpty(Object object) {
		return object == null || object.toString().trim().length() == 0 ? true : false;
	}
	
	public static boolean isNotEmpty(String str) {
		return str != null && str.trim().length() > 0 ? true : false;
	}
	
	public static boolean isNotEmpty(Object object){
		return object != null ? true : false;
	}
	
	/**
	 * 将字符串转换成数组
	 */
	public static String[] spilt(String str) {
		if(str == null)
			throw new NullPointerException("str can not be null");
		return str.split(",");
	}
	
	public static String[] spilt(String str, String separator) {
		if(str == null)
			throw new NullPointerException("str can not be null");
		return str.split(separator);
	}
	
	/**
	 * 将首字母变成小写
	 * @param keyName
	 * @return
	 */
	public static String toLowerCaseFirst(String keyName) {
		if (StrKit.isEmpty(keyName)) 
			throw new NullPointerException(keyName + "can not be null");
		return keyName.substring(0, 1).toLowerCase() + keyName.substring(1, keyName.length());
	}
	
	/**
	 * 将首字母变成大写
	 */
	public static String toUpperCaseFirst(String keyName) {
		if (StrKit.isEmpty(keyName)) 
			throw new NullPointerException(keyName + "can not be null");
		return keyName.substring(0, 1).toUpperCase() + keyName.substring(1, keyName.length());
	}
	
	public static String replaceAll(String str, String regex, String replacement) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, replacement);
        }
        m.appendTail(sb);
        return sb.toString();
    }

	/**
	 * 解析url的 { 和 } 占位符
	 * @param urlMapping
	 * @return
	 */
	public static List<String> parsePlaceholder(String openToken, String closeToke, String urlMapping) {
		int start = urlMapping.indexOf(openToken, 0);
		if (start == -1) {
			return null;
		}
		List<String> params = new ArrayList<>();
		char src[] = urlMapping.toCharArray();
		final StringBuilder builder = new StringBuilder();
		StringBuilder expression = null;
		int offset = 0;
		while (start > -1) {
			if (expression == null) {
				expression = new StringBuilder();
			} else {
				expression.setLength(0);
			}
			builder.append(src, offset, start - offset); //offset 起止位置，
			offset = start + openToken.length();
			int end = urlMapping.indexOf(closeToke, offset);
			if (end > -1) {
				expression.append(src, offset, end - offset);
				offset = end + closeToke.length();
			}
			if (end == -1) {
				builder.append(src, start, src.length - start);
				offset = src.length;
			} else {
				builder.append("?");
				params.add(expression.toString()); //保存参数名称
				offset = end + closeToke.length();
			}
			start = urlMapping.indexOf(openToken, offset); //从offset重新寻找#{字符位置
		}
		return params;
	}
}
